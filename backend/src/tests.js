const {useCollection, createCollection} = require('./models/schema.model');
const {addRouterToCRUD} = require('./_components/CRUDRouter');

module.exports = {
    dynamicController
}

function dynamicController(app){
    tests(app);
}

async function tests(app)
{
    const articleCRUD = await useCollection('article');
    addRouterToCRUD.apply(articleCRUD, [app, {}]);
   
}

async function tests1()
{
    await createCollection({
        code : 'article', collectionSchema: {
            code: { type: "String", required: true },
            description: { type: "String", required: true }
        }
    })
 
    const articleCRUD = await useCollection('article');
    article1 = await articleCRUD.findOne({ code: "art1" });
    const res = await articleCRUD.findOneAndUpdate({ code: "art1" }, { description: "Mise à jour de la description : " + Date.now() })
    console.log("Update Res", res);
    console.log("description", article1.description);
}