const express = require('express');



module.exports =
{
    addRouterToCRUD
}

function addRouterToCRUD (app, opts){
    const router = express.Router();

    function getfunc(req, res, next) {
        console.log('mon This est', this);
        options = req.body.queryOptions || {batchSize: 100};
        filter = req.body.queryFilter || {};
        return this
            .find(filter, null, {...options})
            .exec()
            .then(results => res.status(200).json(results))
            .catch(errors => res.status(400).json(errors))
        
    }

    console.log('Inside addRouterToCRUD');
    router.use(function (req, res, next) {
        console.log(`Received API ${req.method} - path: ${req.path} body:  ${JSON.stringify(req.body)} `);
        console.log('this is :', this); 
        next();
        }.bind(this)
    )

    //get All ressources
    router.get('/', getfunc.bind(this));
    console.log('Bind avec :', this);

    //get specific ressource
    router.get('/:id', function (req, res, next) {
        filter = {_id: req.params.id};
        return this
            .findOne(filter)
            .exec()
            .then(results => res.status(200).json(results))
            .catch(errors => res.status(400).json(errors))
        
    });

    //create a ressource
    router.post('/', function (req, res, next) {
        filter = {_id: req.params.id};
        data = req.body;
        location = req.baseUrl + "/";
        return this
            .create(data)
            .then(document => res.status(201).location(location + document._id).json(document))
            .catch(errors => res.status(401).json(errors))
        
    }.bind(this));

    //update a ressource
    router.put('/:id', function (req, res, next) {
        filter = {_id: req.params.id};
        data = req.body;
        location = req.baseUrl + "/";
        return this.findOneAndUpdate(filter, data)
            .then(document => res.status(201).location(location + document._id).json(document))
            .catch(errors => res.status(404).json(errors))
        
    });

    //delete a ressource
    router.delete('/:id', function (req, res, next) {
        filter = {_id: req.params.id};
        location = req.baseUrl + "/";
        return this.findOneAndDelete(filter)
            .then(document => res.status(document?200:404).json(document||{}))
            .catch(errors => res.status(401).json(errors))
        
    });

    // mount the router on the app
    const ressource = this.modelName;
    app.use(`/${ressource}`, router);

}