const mongoose = require('mongoose');

module.exports = {
    useCollection,
    createCollection
}


const rerentialSchemaDef = {
    code: { type: String, required: true }, //User
    description: { type: String, required: true }, //Users
    collectionCode: { type: String, required: true },
    collectionSchema: { type: String, required: true },
    createDate: { type: Date, default: Date.now, required: true }
};

var models = {};
const refToCreateDef = {
    code: "Schema",
    collectionCode: "schemas",
    collectionSchema: {},
    description: "Metadata of schemas"
}

let rerentialmodel;

function useCollection(code) {
    return getModel(code);
}

function getRerentialModel() {
    if (!rerentialmodel) {
        rerentialSchema = new mongoose.Schema(rerentialSchemaDef);
        return rerentialmodel = mongoose.model("Schema", rerentialSchema);
    }
    else
        return rerentialmodel
}

async function createCollection(data) {
    //Create the collection schema in the collection Referencies and return it's Model
    //It will also create the collection with empty document.
    //Looking in the RerentialSchema
    const rerential = await getRerentialModel();

    return await rerential.findOne({ code: data.code })
        .then(result => {
            if (!result) {
                schemaToCreate = { ...refToCreateDef };
                Object.assign(schemaToCreate, data);
                const collectionSchemaJson = schemaToCreate.collectionSchema;
                schemaToCreate.collectionSchema = JSON.stringify(schemaToCreate.collectionSchema);
                modelNew = new rerentialmodel(schemaToCreate);
                modelNew.collectionCode = modelNew.code + 's';
                return modelNew.save()
                    .then(result => {
                        console.log(`Collection ${data.code}s has been added in Schemas`, result);
                        //Create the collection
                        collectionSchema = new mongoose.Schema(collectionSchemaJson);
                        collectionmodel = mongoose.model(data.code, collectionSchema);
                        return collectionmodel;
                    })
                    .catch(error => console.log(`Error in adding ${data.code}s in Schemas`, error))
            }
            else {
                console.log(`Collection ${data.code}s already exist in Schemas`, result);

                return useModel(data.code, JSON.parse(result.collectionSchema));
            }

        })
}

async function getModel(code) {
    const rerential = getRerentialModel();
    return await rerential.findOne({ code }).
        then(result => {
            if (!result) {
                console.log('model not found');
                return undefined;
            }
            else {
                return useModel(code, JSON.parse(result.collectionSchema));
            }

        })

}


//Create the given schema code if not exist.
function useModel(code, schema) {
    let model = models[code];
    if (!model) {
        collectionSchema = new mongoose.Schema(schema);
        model = mongoose.model(code, collectionSchema);
        models[code] = model;
    }
    return model;
};
