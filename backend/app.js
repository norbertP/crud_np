var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var app = express();
const mongoose = require('mongoose');

const { createCollection } = require('./src/models/schema.model');
const { dynamicController } = require('./src/tests');
const { addRouterToCRUD } = require('./src/_components/CRUDRouter');


app.set('port', 3000);
mongoose.connect('mongodb://localhost:27042/crud_np',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log('Connexion à MongoDB réussie !');
       })
    .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
//app.use('/users', usersRouter);
app.post('/schema', function(req,res) {
        createCollection ( {...req.body})
        .then (newCRUD => {
            try{
                //new addRouterToCRUD(newCRUD, app, {});
                addRouterToCRUD.call(newCRUD, app, {});
                

            res.status(200).send(`New collection '${newCRUD.modelName}' created and its API CRUD availble !`);
            }
            catch(error)
            {
                res.status(400).send("Erreur during create of the collection !");
            }
        })
    
});

dynamicController(app);
module.exports = app;

